# Personal Development API
API that helps track and manage a user's day to day activities through appointments, tasks, and journal entries.

### OpenAPI Specification
[OAS Document](https://app.swaggerhub.com/apis/kbanaag/PersonalDevelopment/1.1.3)

### Technologies Used
* Java 11
* Spring Boot
* Maven
* Keycloak
* Docker
* Jenkins
* JUnit
* JMeter
* Postman

### Database

Entity Relationship Diagram
![](rsc/ERDiagram.png)

##### Users
Have a personalized experience through the Users resource.
```json
{
    "userId": 1,
    "firstName": "Johnny",
    "lastName": "Boy",
    "username": "jboy21",
    "email": "jboy21@hotmail.com",
    "birthday": "1950-02-03",
    "settings": {
      "appAlerts": false,
      "emailNotifications": false,
      "theme": "light"
    },
    "appointments": [
      {
        "appointmentId": 1,
        "userId": 1,
        "date": "2020-12-12",
        "startTime": "2020-12-12 09:00:00",
        "endTime": "2020-12-12 10:00:00",
        "description": "Doctor's appointment",
        "appointmentNotes": "Need more sleep, and a healthier diet."
      }
    ],
    "tasks": [
      {
        "taskId": 1,
        "userId": 1,
        "done": false,
        "name": "Project OAS",
        "deadline": "2021-04-02 10:00:00",
        "progress": 50,
        "priority": 10
      }
    ]
  }
```

##### Appointments
Create and manage prearranged activities through Appointments.
```json
{
    "appointmentId": 1,
    "userId": 1,
    "date": "2020-12-12",
    "startTime": "2020-12-12 09:00:00",
    "endTime": "2020-12-12 10:00:00",
    "description": "Doctor's appointment",
    "appointmentNotes": "Need more sleep, and a healthier diet."
}
```

##### Tasks
Add and Check Off items that need to be accomplished throughout the day through Tasks.
```json
{
    "taskId": 1,
    "userId": 1,
    "done": false,
    "name": "Project OAS",
    "deadline": "2021-04-02 10:00:00",
    "progress": 50,
    "priority": 10
 }
```

##### Journal Entries
Log end of the day thoughts and experiences through Journal Entries.
```json
{
    "entryId": 1,
    "userId": 1,
    "date": "2021-04-02",
    "summary": "Something about the user's day.",
    "positives": "Some positive things that happened today",
    "moodScore": 95
}
```

### How do I get set up?

For IntelliJIDEA
1. Clone repository
    ```sh
    git clone https://kenzobanaag@bitbucket.org/mountainstatesoftware/kenzo-banaag-basecamp-project.git 
    ```
2. Open IntelliJIDEA
3. Import Project from Existing Source... and locate pom.xml <br/>

#### Run Locally

There are a couple of ways of running the project locally:
1. Open cmd in project directory and run ``` docker-compose up ``` and wait for everything to build and load.
    
2. Run the database, then run keycloak and finally run this project.

#### Set Up and Run CI/CD Pipeline with Jenkins

1. Select New Item
2. Click on Pipeline then Click OK
3. Go to Configure
4. Go to the Pipeline Section then select 'Pipeline script from SCM'
5. Select Git as SCM 
6. Enter ssh repository URL: 
    ```sh
    git@bitbucket.org:mountainstatesoftware/kenzo-banaag-basecamp-project.git 
    ```
7. Select bitbucket for credentials
8. Add */develop to branch specifier
9. Add ci/Jenkinsfile to Script Path then save
10. Click Build Now and wait for it to finish

#### Pull Image from Docker Registry and Run

To pull the latest image from MS3 Docker Registry
```sh
docker pull docker-public.kube.cloudapps.ms3-inc.com/kbanaag-pd-api:latest 
# Check if image was pulled
docker images
# Check if running
docker ps
# Pull and run image (Alternative)
docker run docker-public.kube.cloudapps.ms3-inc.com/kbanaag-pd-api:latest 
```

Pull and run the whole project in one line
```sh
docker-compose up
```
