package com.banaag.personaldev.services;

import com.banaag.personaldev.model.User;

import java.util.Optional;

public interface UserService {
    Iterable<User> getUsers();
    Optional<User> getUserById(long id);
    long addUser(User user);
    long updateUser(long id, User user);
    void deleteUserById(long id);
}
