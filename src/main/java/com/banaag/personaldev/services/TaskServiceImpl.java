package com.banaag.personaldev.services;

import com.banaag.personaldev.exception.BadRequestException;
import com.banaag.personaldev.exception.ResourceNotFoundException;
import com.banaag.personaldev.model.Task;
import com.banaag.personaldev.model.User;
import com.banaag.personaldev.model.metamodels.TaskMeta;
import com.banaag.personaldev.repository.TaskRepository;
import com.banaag.personaldev.repository.UserRepository;
import com.banaag.personaldev.specifications.TaskSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class TaskServiceImpl implements TaskService {

    @Autowired
    UserRepository userRepo;

    @Autowired
    TaskRepository taskRepo;

    @Autowired
    TaskSpecification taskSpec;

    @Override
    public Iterable<Task> getTasks(TaskMeta taskMeta) {
        validateUserExists(taskMeta.getUserId());
        return taskRepo.findAll(taskSpec.taskQuery(taskMeta));
    }

    @Override
    public Optional<Task> getTaskById(long userId, long taskId) {
        return taskRepo.findTaskByUserUserIdAndTaskId(userId, taskId);
    }

    @Override
    public long addTask(long userId, Task task) {
        Optional<User> tempUser = userRepo.findById(userId);
        if(tempUser.isEmpty())
            throw new ResourceNotFoundException("User doesn't exist");
        validateComplete(task);
        validateRangeInput(task);
        User user = tempUser.get();
        task.setUser(user);
        user.getTasks().add(task);
        userRepo.save(user);
        return user.getTasks().size() > 0 ? user.getTasks().get(user.getTasks().size() - 1).getTaskId() : -1;
        // return the added task by getting the last item on the list
    }

    @Override
    public long updateTask(long userId, long taskId, Task task) {
        Optional<User> tempUser = userRepo.findById(userId);
        if(tempUser.isEmpty())
            throw new ResourceNotFoundException("User doesn't exist");
        Optional<Task> tempTask = taskRepo.findById(taskId);
        if(tempTask.isEmpty())
            throw new ResourceNotFoundException("Task doesn't exist");
        validateRangeInput(task);
        User user = tempUser.get();
        Task tsk = tempTask.get();
        int index = user.getTasks().indexOf(tsk);
        tsk.patch(task);
        user.getTasks().set(index, tsk);
        userRepo.save(user);
        return taskId;
    }

    @Override
    public void deleteTaskById(long userId, long taskId) {
        validateUserExists(userId);
        validateTaskExists(taskId);
        taskRepo.deleteById(taskId);
    }

    @Override
    public void deleteAllTasksById(long userId, Optional<Boolean> done) {
        validateUserExists(userId);
        taskRepo.deleteAllByUserUserId(userId, done);
    }

    private void validateUserExists(long userId) {
        if(!userRepo.existsById(userId))
            throw new ResourceNotFoundException("User doesn't exist");
    }

    private void validateTaskExists(long taskId) {
        if(!taskRepo.existsById(taskId))
            throw new ResourceNotFoundException("Task doesn't exist");
    }

    private void validateComplete(Task task) {
        if(!task.isComplete())
            throw new BadRequestException("Missing required field(s)");
    }

    private void validateRangeInput(Task task) {
        if(!task.isValid())
            throw new BadRequestException("Check range input for integer fields");
    }
}
