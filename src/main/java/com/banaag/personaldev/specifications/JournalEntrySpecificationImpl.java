package com.banaag.personaldev.specifications;

import com.banaag.personaldev.model.JournalEntry;
import com.banaag.personaldev.model.metamodels.JournalEntryMeta;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

@Component
public class JournalEntrySpecificationImpl implements JournalEntrySpecification {
    @Override
    public Specification<JournalEntry> journalQuery(JournalEntryMeta metaModel) {
        return (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();

            predicates.add(criteriaBuilder.equal(root.get("user").get("userId"), metaModel.getUserId()));
            if(metaModel.getMoodScore().isPresent())
                predicates.add(criteriaBuilder.equal(root.get("moodScore"), metaModel.getMoodScore().get()));

            if(metaModel.getDate().isPresent())
                predicates.add(criteriaBuilder.equal(root.get("date"), metaModel.getDate().get()));

            if(metaModel.getFromMoodScore().isPresent() && metaModel.getToMoodScore().isPresent()
                    && metaModel.getFromMoodScore().get() < metaModel.getToMoodScore().get()) {
                predicates.add(criteriaBuilder.between(root.get("moodScore"),
                        metaModel.getFromMoodScore().get(), metaModel.getToMoodScore().get()));
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }
}
