package com.banaag.personaldev.specifications;

import com.banaag.personaldev.model.JournalEntry;
import com.banaag.personaldev.model.metamodels.JournalEntryMeta;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

@Component
public interface JournalEntrySpecification {
    Specification<JournalEntry> journalQuery(JournalEntryMeta metaModel);
}
