package com.banaag.personaldev.specifications;

import com.banaag.personaldev.model.Task;
import com.banaag.personaldev.model.metamodels.TaskMeta;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

@Component
public class TaskSpecificationImpl implements TaskSpecification {
    @Override
    public Specification<Task> taskQuery(TaskMeta taskMeta) {
        return (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();

            predicates.add(criteriaBuilder.equal(root.get("user").get("userId"), taskMeta.getUserId()));
            if(taskMeta.getDone().isPresent())
                predicates.add(criteriaBuilder.equal(root.get("done"), taskMeta.getDone().get()));

            if(taskMeta.getPriority().isPresent())
                predicates.add(criteriaBuilder.equal(root.get("priority"), taskMeta.getPriority().get()));

            if(taskMeta.getProgress().isPresent())
                predicates.add(criteriaBuilder.equal(root.get("progress"), taskMeta.getProgress().get()));

            if(taskMeta.getDeadline().isPresent())
                predicates.add(criteriaBuilder.equal(root.get("deadline"), taskMeta.getDeadline().get()));

            if(taskMeta.getPriority().isPresent())
                predicates.add(criteriaBuilder.equal(root.get("priority"), taskMeta.getPriority().get()));

            if(taskMeta.getPriorityGTE().isPresent())
                predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get("priority"),
                        taskMeta.getPriorityGTE().get()));

            if(taskMeta.getFromProgress().isPresent() && taskMeta.getToProgress().isPresent()
                    && taskMeta.getFromProgress().get() < taskMeta.getToProgress().get()) {
                predicates.add(criteriaBuilder.between(root.get("progress"),
                        taskMeta.getFromProgress().get(), taskMeta.getToProgress().get()));
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }
}
