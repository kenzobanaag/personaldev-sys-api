package com.banaag.personaldev.controller;

import com.banaag.personaldev.model.Task;
import com.banaag.personaldev.model.metamodels.TaskMeta;
import com.banaag.personaldev.services.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Date;
import java.util.Optional;

@RestController
public class TaskController {

    @Autowired
    TaskService taskService;

    @GetMapping("/users/{userId}/tasks")
    public ResponseEntity<Object> getTasks(@PathVariable("userId") long userId,
                                   @RequestParam(required = false) Optional<Boolean> done,
                                   @RequestParam(required = false) Optional<Integer> priority,
                                   @RequestParam(required = false) Optional<Integer> progress,
                                   @RequestParam(required = false) Optional<Date> deadline,
                                   @RequestParam(required = false) Optional<Integer> priorityGTE,
                                   @RequestParam(required = false) Optional<Integer> fromProgress,
                                   @RequestParam(required = false) Optional<Integer> toProgress) {
        return ResponseEntity.ok(taskService.getTasks(
                new TaskMeta(userId, done, priority, progress, deadline, priorityGTE, fromProgress, toProgress)));
    }

    @PostMapping("/users/{userId}/tasks")
    public ResponseEntity<Object> addTask(@PathVariable("userId") long userId, @RequestBody Task task) {
        long createdTaskId = taskService.addTask(userId, task);
        try {
            HttpHeaders header = new HttpHeaders();
            header.add(HttpHeaders.LOCATION,
                    "/users/"+userId+"/tasks/"+createdTaskId);
            return ResponseEntity
                    .created(new URI("/users/"+userId+"/tasks"+createdTaskId))
                    .headers(header)
                    .build();
        } catch (URISyntaxException e) {
            throw new RuntimeException("Error in POST /tasks");
        }
    }

    @DeleteMapping("/users/{userId}/tasks")
    public ResponseEntity<Object> deleteTasks(@PathVariable("userId") long userId,
                                              @RequestParam(required = false) Optional<Boolean> done) {
        taskService.deleteAllTasksById(userId, done);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/users/{userId}/tasks/{taskId}")
    public ResponseEntity<Object> getTaskById(@PathVariable("userId") long userId,
                                              @PathVariable("taskId") long taskId) {
        Optional<Task> task = taskService.getTaskById(userId, taskId);
        if(task.isPresent())
            return ResponseEntity.ok().body(task);
        else
            return ResponseEntity.notFound().build();
    }

    @PatchMapping("/users/{userId}/tasks/{taskId}")
    public ResponseEntity<Object> updateTask(@PathVariable("userId") long userId,
                                             @PathVariable("taskId") long taskId,
                                             @RequestBody Task task) {
        long tempTaskId = taskService.updateTask(userId, taskId, task);
        HttpHeaders header = new HttpHeaders();
        header.add(HttpHeaders.LOCATION, "/users/"+userId+"/tasks/"+tempTaskId);
        return ResponseEntity
                .ok()
                .headers(header)
                .build();
    }

    @DeleteMapping("/users/{userId}/tasks/{taskId}")
    public ResponseEntity<Object> deleteTaskById(@PathVariable("userId") long userId,
                                                 @PathVariable("taskId") long taskId) {
        taskService.deleteTaskById(userId, taskId);
        return ResponseEntity.noContent().build();
    }
}