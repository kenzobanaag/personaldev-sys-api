package com.banaag.personaldev.controller;

import com.banaag.personaldev.model.JournalEntry;
import com.banaag.personaldev.model.metamodels.JournalEntryMeta;
import com.banaag.personaldev.services.JournalEntryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Date;
import java.util.Optional;

@RestController
public class JournalEntryController {

    @Autowired
    JournalEntryService journalEntryService;

    @GetMapping("/users/{userId}/journal-entries")
    public ResponseEntity<Object> getJournalEntries(@PathVariable("userId") long userId,
                                            @RequestParam(required = false) Optional<Date> date,
                                            @RequestParam(required = false) Optional<Integer> moodScore,
                                            @RequestParam(required = false) Optional<Integer> fromMoodScore,
                                            @RequestParam(required = false) Optional<Integer> toMoodScore) {
        return ResponseEntity.ok(journalEntryService.getJournalEntries(
                new JournalEntryMeta(userId, date, moodScore,fromMoodScore,toMoodScore)));
    }

    @PostMapping("/users/{userId}/journal-entries")
    public ResponseEntity<Object> addJournalEntry(@PathVariable("userId") long userId,
                                                  @RequestBody JournalEntry entry) {
        long createdEntryId = journalEntryService.addJournalEntry(userId, entry);
        try {
            HttpHeaders header = new HttpHeaders();
            header.add(HttpHeaders.LOCATION,
                    "/users/"+userId+"/journal-entries/"+createdEntryId);
            return ResponseEntity
                    .created(new URI("/users/"+userId+"/journal-entries"+createdEntryId))
                    .headers(header)
                    .build();
        } catch (URISyntaxException e) {
            throw new RuntimeException("Error in POST /journal-entries");
        }
    }

    @GetMapping("/users/{userId}/journal-entries/{entryId}")
    public ResponseEntity<Object> getJournalEntryById(@PathVariable("userId") long userId,
                                                      @PathVariable("entryId") long entryId) {
        Optional<JournalEntry> entry = journalEntryService.getJournalEntryById(userId, entryId);
        if(entry.isPresent())
            return ResponseEntity.ok().body(entry.get());
        else
            return ResponseEntity.notFound().build();
    }

    @PatchMapping("/users/{userId}/journal-entries/{entryId}")
    public ResponseEntity<Object> updateJournalEntry(@PathVariable("userId") long userId,
                                                     @PathVariable("entryId") long entryId,
                                                     @RequestBody JournalEntry entry) {
        long tempEntryId = journalEntryService.updateJournalEntry(userId, entryId,entry);
        HttpHeaders header = new HttpHeaders();
        header.add(HttpHeaders.LOCATION, "/users/"+userId+"/journal-entries/"+tempEntryId);
        return ResponseEntity
                .ok()
                .headers(header)
                .build();
    }

    @DeleteMapping("/users/{userId}/journal-entries/{entryId}")
    public ResponseEntity deleteJournalEntryById(@PathVariable("userId") long userId,
                                                 @PathVariable("entryId") long entryId) {
        journalEntryService.deleteJournalEntryById(userId, entryId);
        return ResponseEntity.noContent().build();
    }
}
