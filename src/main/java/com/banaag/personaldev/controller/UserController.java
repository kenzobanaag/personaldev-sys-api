package com.banaag.personaldev.controller;

import com.banaag.personaldev.model.User;
import com.banaag.personaldev.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;

@RestController
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/users")
    public ResponseEntity<Iterable<User>> getUsers() {
        return ResponseEntity.ok().body(userService.getUsers());
    }

    @PostMapping("/users")
    public ResponseEntity<Object> addUser(@RequestBody User user) {
        long createdUserId = userService.addUser(user);
        try {
            HttpHeaders header = new HttpHeaders();
            header.add(HttpHeaders.LOCATION, "/users/"+createdUserId);
            return ResponseEntity
                    .created(new URI("/users"))
                    .headers(header)
                    .build();
        } catch (URISyntaxException e) {
            throw new RuntimeException("Error in POST /users");
        }
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<Optional<User>> getUserById(@PathVariable("id") long id) {
        Optional<User> user = userService.getUserById(id);
        if(user.isPresent())
            return ResponseEntity.ok().body(user);
        else
            return ResponseEntity.notFound().build();
    }

    @PatchMapping("/users/{id}")
    public ResponseEntity<Object> updateUser(@PathVariable("id") long id, @RequestBody User user) {
        long patchedUserId = userService.updateUser(id,user);
        HttpHeaders header = new HttpHeaders();
        header.add(HttpHeaders.LOCATION, "/users/"+patchedUserId);
        return ResponseEntity
                .ok()
                .headers(header)
                .build();
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<Object> deleteUserById(@PathVariable("id") long id) {
        userService.deleteUserById(id);
        return ResponseEntity.noContent().build();
    }
}
