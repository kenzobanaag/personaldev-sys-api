package com.banaag.personaldev.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Data
@Table(name = "journal_entries_table")
public class JournalEntry implements Patchable<JournalEntry> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long entryId;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private User user; // owner of this entry

    private Date date;
    private String summary;
    private String positives;
    private int moodScore;

    @JsonIgnore
    public boolean isComplete() {
        return date != null && summary != null;
    }

    @Override
    public void patch(JournalEntry entry) {
        if(entry.date != null)
            date = entry.date;
        if(entry.summary != null)
            summary = entry.summary;
        if(entry.positives != null)
            positives = entry.positives;
        if(entry.moodScore != 0)
            moodScore = entry.moodScore;
    }
}