package com.banaag.personaldev.model;

/*
* NOTES:
* For future reference, if we find that having collections under a user is too slow or under performing, (ie. OneToMany)
* we should take the advice that "you are better off replacing collections with a query, which is much more flexible in
* terms of fetching performance"
* From: https://vladmihalcea.com/the-best-way-to-map-a-onetomany-association-with-jpa-and-hibernate/
* This approach will remove unnecessary bidirectional associations between the user and other collections.
* */
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Entity
@Data
@Table(name="users_table")
public class User implements Patchable<User> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long userId;
    @Column(nullable = false) // Should not allow null values in this column
    private String firstName;
    @Column(nullable = false)
    private String lastName;
    @Column(nullable = false)
    private String username;
    @Column(nullable = false)
    private String email;
    @Column(nullable = false)
    private Date birthday;

    @OneToOne(mappedBy = "user", fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private Settings settings;

    // Using lists so we don't have to implement some type of search for when were trying to get the id of a
    // specific appointment or task. Ie: After adding, just get the last item on list, if we were to use a set
    // we might have to implement something more complex just to get the id of the added item.
    // many to one is most efficient but we need this reference. Join column on child makes db performance a lil better.
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<Appointment> appointments;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    private List<Task> tasks;

    @JsonIgnore
    public boolean isComplete() {
        return firstName != null && lastName != null && username != null && email != null && birthday != null;
    }

    public void patch(User user) {
        if(user.firstName != null)
            firstName = user.firstName;
        if(user.lastName != null)
            lastName = user.lastName;
        if(user.username != null)
            username = user.username;
        if(user.email != null)
            email = user.email;
        if(user.birthday != null)
            birthday = user.birthday;
    }
}