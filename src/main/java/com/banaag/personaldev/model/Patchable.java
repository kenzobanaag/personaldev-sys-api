package com.banaag.personaldev.model;

public interface Patchable<T> {
    void patch(T obj);
}