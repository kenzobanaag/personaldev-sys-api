package com.banaag.personaldev.repository;

import com.banaag.personaldev.model.JournalEntry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

public interface JournalEntryRepository extends JpaRepository<JournalEntry, Long>,
        JpaSpecificationExecutor<JournalEntry> {
    Optional<JournalEntry> findByUserUserIdAndEntryId(long userId, long entryId);
}
