package com.banaag.personaldev.repository;

import com.banaag.personaldev.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface TaskRepository extends JpaRepository<Task, Long>, JpaSpecificationExecutor<Task> {
    @Modifying
    @Transactional
    @Query(value = "DELETE FROM tasks_table WHERE user_id = ?1 AND (?2 IS NULL OR done = ?2)",
            nativeQuery = true)
    void deleteAllByUserUserId(long userId, Optional<Boolean> done);
    Optional<Task> findTaskByUserUserIdAndTaskId(long userId, long taskId);
}
