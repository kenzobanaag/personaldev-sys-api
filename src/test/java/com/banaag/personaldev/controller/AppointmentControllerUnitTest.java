package com.banaag.personaldev.controller;

import com.banaag.personaldev.model.Appointment;
import com.banaag.personaldev.services.AppointmentService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.sql.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(AppointmentController.class)
public class AppointmentControllerUnitTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper mapper;

    @MockBean
    AppointmentService appointmentService;

    private static Appointment appt;

    @BeforeAll
    public static void setup() {
        appt = new Appointment();
        appt.setDate(new Date(System.currentTimeMillis()));
        appt.setStartTime(new Date(System.currentTimeMillis()));
        appt.setEndTime(new Date(System.currentTimeMillis()));
    }

    @Test
    public void controllerInitTest() {
        assertNotNull(mockMvc);
    }

    @Test
    public void getAllAppointments() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.get("/users/1/appointments")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string("[]"));
    }

    @Test
    public void getAppointmentById() throws Exception{
        Optional<Appointment> optUser = Optional.of(appt);
        when(appointmentService.getAppointmentById(1L, 1L)).thenReturn(optUser);
        String apptJsonStr = this.mapper.writeValueAsString(appt);

        mockMvc.perform(MockMvcRequestBuilders.get("/users/1/appointments/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(apptJsonStr));

        verify(appointmentService, times(1)).getAppointmentById(1L,1L);
    }

    @Test
    public void getAppointmentById_userNotExist() throws Exception{
        when(appointmentService.getAppointmentById(0L, 1L)).thenReturn(Optional.empty());

        mockMvc.perform(MockMvcRequestBuilders.get("/users/0/appointments/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

        verify(appointmentService, times(1)).getAppointmentById(0L,1L);
    }

    @Test
    public void getAppointmentById_idNotExist() throws Exception{
        when(appointmentService.getAppointmentById(1L, 0L)).thenReturn(Optional.empty());

        mockMvc.perform(MockMvcRequestBuilders.get("/users/1/appointments/0")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

        verify(appointmentService, times(1)).getAppointmentById(1L,0L);
    }

    @Test
    public void addAppointment() throws Exception{
        String apptJsonStr = this.mapper.writeValueAsString(appt);

        ResultActions result = mockMvc.perform(MockMvcRequestBuilders.post("/users/1/appointments")
                .contentType(MediaType.APPLICATION_JSON).content(apptJsonStr)).andExpect(status().isCreated());

        assertEquals("/users/1/appointments/0", result.andReturn().getResponse().getHeader("Location"));
    }

    @Test
    public void updateAppointment() throws Exception {
        String apptJsonStr = this.mapper.writeValueAsString(appt);

        ResultActions result = mockMvc.perform(MockMvcRequestBuilders.patch("/users/1/appointments/0")
                .contentType(MediaType.APPLICATION_JSON).content(apptJsonStr)).andExpect(status().isOk());

        assertEquals("/users/1/appointments/0", result.andReturn().getResponse().getHeader("Location"));
    }

    @Test
    public void deleteAppointment() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.delete("/users/1/appointments/1"))
                .andExpect(status().isNoContent());
        verify(appointmentService, times(1)).deleteAppointmentById(1L, 1L);
    }
}
