package com.banaag.personaldev.controller;

import com.banaag.personaldev.model.User;
import com.banaag.personaldev.services.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(UserController.class)
public class UserControllerUnitTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper mapper;

    @MockBean
    UserService userService;

    private static User user1;
    private static User user2;

    @BeforeAll
    public static void setup() {
        user1 = new User();
        user1.setFirstName("Test1");
        user1.setLastName("Test1");
        user1.setUsername("test1");
        user1.setEmail("test1@email.com");
        user1.setBirthday(new Date(System.currentTimeMillis()));

        user2 = new User();
        user2.setFirstName("Test2");
        user2.setLastName("Test2");
        user2.setUsername("test2");
        user2.setEmail("test2@email.com");
        user2.setBirthday(new Date(System.currentTimeMillis()));
    }

    @Test
    public void controllerInitTest() {
        assertNotNull(mockMvc);
    }

    @Test
    public void getAllUsers() throws Exception {
        List<User> users = new ArrayList<>();
        users.add(user1); users.add(user2);
        when(userService.getUsers()).thenReturn(users);
        String usersJsonStr = this.mapper.writeValueAsString(users);

        mockMvc.perform(MockMvcRequestBuilders.get("/users")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(usersJsonStr))
                .andExpect(jsonPath("$", hasSize(2)));

        verify(userService, times(1)).getUsers();
    }

    @Test
    public void getUserById() throws Exception{
        Optional<User> optUser = Optional.of(user1);
        when(userService.getUserById(1L)).thenReturn(optUser);
        String userJsonStr = this.mapper.writeValueAsString(user1);

        mockMvc.perform(MockMvcRequestBuilders.get("/users/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(userJsonStr));

        verify(userService, times(1)).getUserById(1L);
    }

    @Test
    public void getUserById_idNotExist() throws Exception{
        when(userService.getUserById(1L)).thenReturn(Optional.empty());

        mockMvc.perform(MockMvcRequestBuilders.get("/users/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

        verify(userService, times(1)).getUserById(1L);
    }

    @Test
    public void addUser() throws Exception {
        String userJsonStr = this.mapper.writeValueAsString(user1);

        ResultActions result = mockMvc.perform(MockMvcRequestBuilders.post("/users")
                .contentType(MediaType.APPLICATION_JSON).content(userJsonStr)).andExpect(status().isCreated());

        assertEquals("/users/0", result.andReturn().getResponse().getHeader("Location"));
    }

    @Test
    public void updateUser() throws Exception{
        String userJsonStr = this.mapper.writeValueAsString(user1);

        ResultActions result = mockMvc.perform(MockMvcRequestBuilders.patch("/users/0")
                .contentType(MediaType.APPLICATION_JSON).content(userJsonStr))
                .andExpect(status().isOk());

        assertEquals("/users/0", result.andReturn().getResponse().getHeader("Location"));
    }

    @Test
    public void deleteUserById() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.delete("/users/1"))
                .andExpect(status().isNoContent());
        verify(userService, times(1)).deleteUserById(1L);
    }
}