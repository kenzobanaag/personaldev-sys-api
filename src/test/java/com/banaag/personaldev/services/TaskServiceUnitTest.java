package com.banaag.personaldev.services;

import com.banaag.personaldev.exception.BadRequestException;
import com.banaag.personaldev.exception.ResourceNotFoundException;
import com.banaag.personaldev.model.Task;
import com.banaag.personaldev.model.User;
import com.banaag.personaldev.model.metamodels.TaskMeta;
import com.banaag.personaldev.repository.TaskRepository;
import com.banaag.personaldev.repository.UserRepository;
import com.banaag.personaldev.specifications.TaskSpecification;
import org.assertj.core.util.IterableUtil;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@ExtendWith(SpringExtension.class)
public class TaskServiceUnitTest {

    @InjectMocks
    TaskServiceImpl taskService;

    @Mock
    TaskRepository taskRepo;

    @Mock
    UserRepository userRepo;

    @Mock
    TaskSpecification taskSpec;

    @Test
    public void serviceInitTest() {
        assertNotNull(taskService);
    }

    @Test
    public void getTasks_empty() {
        TaskMeta meta = mock(TaskMeta.class);
        when(taskSpec.taskQuery(meta)).thenReturn(any(Specification.class));
        when(taskRepo.findAll(taskSpec.taskQuery(meta))).thenReturn(new ArrayList<>());
        when(userRepo.existsById(meta.getUserId())).thenReturn(true);

        assertEquals(0, IterableUtil.sizeOf(taskService.getTasks(meta)));
        verify(taskRepo, times(1)).findAll(taskSpec.taskQuery(meta));
    }

    @Test
    public void getTasks() {
        List<Task> tasks = new ArrayList<>();
        tasks.add(mock(Task.class));
        tasks.add(mock(Task.class));
        TaskMeta meta = mock(TaskMeta.class);
        when(taskSpec.taskQuery(meta)).thenReturn(any(Specification.class));
        when(taskRepo.findAll(taskSpec.taskQuery(meta))).thenReturn(tasks);
        when(userRepo.existsById(meta.getUserId())).thenReturn(true);

        assertEquals(tasks.size(), IterableUtil.sizeOf(taskService.getTasks(meta)));
        verify(taskRepo, times(1)).findAll(taskSpec.taskQuery(meta));
    }

    @Test
    public void getTasksById() {
        long userId = 1L;
        long taskId = 1L;
        when(taskRepo.findTaskByUserUserIdAndTaskId(userId, taskId))
                .thenReturn(Optional.of(mock(Task.class)));

        Optional<Task> userFromService = taskService.getTaskById(userId, taskId);
        assertTrue(userFromService.isPresent());
        verify(taskRepo, times(1)).findTaskByUserUserIdAndTaskId(userId, taskId);
    }

    @Test
    public void getTasksById_userNotExist() {
        long userId = 1L;
        long taskId = 1L;
        when(taskRepo.findTaskByUserUserIdAndTaskId(userId, taskId))
                .thenReturn(Optional.empty());

        Optional<Task> userFromService = taskService.getTaskById(userId, taskId);
        assertTrue(userFromService.isEmpty());
        verify(taskRepo, times(1)).findTaskByUserUserIdAndTaskId(userId, taskId);
    }

    @Test
    public void getTasksById_idNotExist() {
        long userId = 1L;
        long taskId = 1L;
        when(taskRepo.findTaskByUserUserIdAndTaskId(userId, taskId))
                .thenReturn(Optional.empty());

        Optional<Task> userFromService = taskService.getTaskById(userId, taskId);
        assertTrue(userFromService.isEmpty());
        verify(taskRepo, times(1)).findTaskByUserUserIdAndTaskId(userId, taskId);
    }

    @Test
    public void addTask() {
        long userId = 1L;
        List<Task> tasks = new ArrayList<>();
        Task task = mock(Task.class);
        tasks.add(task);
        User user = mock(User.class);
        when(user.getTasks()).thenReturn(tasks);
        when(task.isComplete()).thenReturn(true);
        when(task.isValid()).thenReturn(true);
        when(task.getTaskId()).thenReturn(1L);

        when(userRepo.findById(userId)).thenReturn(Optional.of(user));

        assertTrue(taskService.addTask(userId, task) > 0);
        verify(userRepo, times(1)).findById(userId);
        verify(userRepo, times(1)).save(user);
    }

    @Test
    public void addTask_userNotExist() {
        long userId = 1L;
        Task task = mock(Task.class);
        User user = mock(User.class);

        when(userRepo.findById(userId)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> taskService.addTask(userId, task));
        verify(userRepo, times(1)).findById(userId);
        verify(userRepo, times(0)).save(user);
    }

    @Test
    public void addTask_incomplete() {
        long userId = 1L;
        Task task = mock(Task.class);
        User user = mock(User.class);

        when(task.isComplete()).thenReturn(false);
        when(task.isValid()).thenReturn(true);

        when(userRepo.findById(userId)).thenReturn(Optional.of(user));

        assertThrows(BadRequestException.class, () -> taskService.addTask(userId, task));
        verify(userRepo, times(1)).findById(userId);
        verify(userRepo, times(0)).save(user);
    }

    @Test
    public void addTask_invalid() {
        long userId = 1L;
        Task task = mock(Task.class);
        User user = mock(User.class);

        when(task.isComplete()).thenReturn(true);
        when(task.isValid()).thenReturn(false);

        when(userRepo.findById(userId)).thenReturn(Optional.of(user));

        assertThrows(BadRequestException.class, () -> taskService.addTask(userId, task));
        verify(userRepo, times(1)).findById(userId);
        verify(userRepo, times(0)).save(user);
    }

    @Test
    public void addTask_sizeZero() {
        long userId = 1L;
        List<Task> tasks = mock(ArrayList.class);
        Task task = mock(Task.class);
        User user = mock(User.class);
        when(tasks.size()).thenReturn(0);
        when(user.getTasks()).thenReturn(tasks);
        when(task.isComplete()).thenReturn(true);
        when(task.isValid()).thenReturn(true);

        when(userRepo.findById(userId)).thenReturn(Optional.of(user));

        assertFalse(taskService.addTask(userId, task) > 0);
        verify(userRepo, times(1)).findById(userId);
        verify(userRepo, times(1)).save(user);
    }

    @Test
    public void updateTask() {
        long userId = 1L;
        long taskId = 1L;
        List<Task> tasks = new ArrayList<>();
        Task task = mock(Task.class);
        tasks.add(task);
        User user = mock(User.class);
        when(user.getTasks()).thenReturn(tasks);
        when(task.isValid()).thenReturn(true);

        when(userRepo.findById(userId)).thenReturn(Optional.of(user));
        when(taskRepo.findById(taskId)).thenReturn(Optional.of(task));

        assertEquals(taskId, taskService.updateTask(userId,taskId, task));
        verify(userRepo, times(1)).findById(userId);
        verify(taskRepo, times(1)).findById(taskId);
        verify(userRepo, times(1)).save(user);
    }

    @Test
    public void updateTask_userNotExist() {
        long userId = 1L;
        long taskId = 1L;
        Task task = mock(Task.class);
        User user = mock(User.class);

        when(userRepo.findById(userId)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> taskService.updateTask(userId,taskId, task));
        verify(userRepo, times(1)).findById(userId);
        verify(taskRepo, times(0)).findById(taskId);
        verify(userRepo, times(0)).save(user);
    }

    @Test
    public void updateTask_idNotExist() {
        long userId = 1L;
        long taskId = 1L;

        Task task = mock(Task.class);
        User user = mock(User.class);

        when(task.isValid()).thenReturn(true);

        when(userRepo.findById(userId)).thenReturn(Optional.of(user));
        when(taskRepo.findById(taskId)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> taskService.updateTask(userId,taskId, task));
        verify(userRepo, times(1)).findById(userId);
        verify(taskRepo, times(1)).findById(taskId);
        verify(userRepo, times(0)).save(user);
    }

    @Test
    public void updateTask_invalid() {
        long userId = 1L;
        long taskId = 1L;

        Task task = mock(Task.class);
        User user = mock(User.class);

        when(task.isValid()).thenReturn(false);
        when(userRepo.findById(userId)).thenReturn(Optional.of(user));
        when(taskRepo.findById(taskId)).thenReturn(Optional.of(task));

        assertThrows(BadRequestException.class, () -> taskService.updateTask(userId,taskId, task));
        verify(userRepo, times(1)).findById(userId);
        verify(taskRepo, times(1)).findById(taskId);
        verify(userRepo, times(0)).save(user);
    }

    @Test
    public void deleteTaskById() {
        long userId = 1L;
        long taskId = 1L;

        when(userRepo.existsById(userId)).thenReturn(true);
        when(taskRepo.existsById(taskId)).thenReturn(true);

        assertDoesNotThrow(() -> taskService.deleteTaskById(userId, taskId));
        verify(userRepo, times(1)).existsById(userId);
        verify(taskRepo, times(1)).existsById(taskId);
        verify(taskRepo, times(1)).deleteById(taskId);
    }

    @Test
    public void deleteTaskById_idNotExist() {
        long userId = 1L;
        long taskId = 1L;

        when(userRepo.existsById(userId)).thenReturn(false);
        when(taskRepo.existsById(taskId)).thenReturn(true);

        assertThrows(ResourceNotFoundException.class,
                () -> taskService.deleteTaskById(userId, taskId));
        verify(userRepo, times(1)).existsById(userId);
        verify(taskRepo, times(0)).existsById(taskId);
        verify(taskRepo, times(0)).deleteById(taskId);
    }

    @Test
    public void deleteTaskById_userNotExist() {
        long userId = 1L;
        long taskId = 1L;

        when(userRepo.existsById(userId)).thenReturn(true);
        when(taskRepo.existsById(taskId)).thenReturn(false);

        assertThrows(ResourceNotFoundException.class,
                () -> taskService.deleteTaskById(userId, taskId));

        verify(userRepo, times(1)).existsById(userId);
        verify(taskRepo, times(1)).existsById(taskId);
        verify(taskRepo, times(0)).deleteById(taskId);
    }

    @Test
    public void deleteAllTasksById() {
        long userId = 1L;

        when(userRepo.existsById(userId)).thenReturn(true);

        assertDoesNotThrow(() -> taskService.deleteAllTasksById(userId, Optional.empty()));

        verify(userRepo, times(1)).existsById(userId);
        verify(taskRepo, times(1)).deleteAllByUserUserId(userId, Optional.empty());
    }

    @Test
    public void deleteAllTasksById_userNotExist() {
        long userId = 1L;

        when(userRepo.existsById(userId)).thenReturn(false);

        assertThrows(ResourceNotFoundException.class,
                () -> taskService.deleteAllTasksById(userId, Optional.empty()));

        verify(userRepo, times(1)).existsById(userId);
        verify(taskRepo, times(0)).deleteAllByUserUserId(userId, Optional.empty());
    }
}