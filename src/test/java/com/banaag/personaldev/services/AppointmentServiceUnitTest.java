package com.banaag.personaldev.services;

import com.banaag.personaldev.exception.BadRequestException;
import com.banaag.personaldev.exception.ResourceNotFoundException;
import com.banaag.personaldev.model.Appointment;
import com.banaag.personaldev.model.User;
import com.banaag.personaldev.model.metamodels.AppointmentMeta;
import com.banaag.personaldev.repository.AppointmentRepository;
import com.banaag.personaldev.repository.UserRepository;
import com.banaag.personaldev.specifications.AppointmentSpecification;
import org.assertj.core.util.IterableUtil;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@ExtendWith(SpringExtension.class)
public class AppointmentServiceUnitTest {

    @InjectMocks
    AppointmentServiceImpl appointmentService;

    @Mock
    AppointmentRepository appointmentRepo;

    @Mock
    UserRepository userRepo;

    @Mock
    AppointmentSpecification appointmentSpec;

    @Test
    public void serviceInitTest() {
        assertNotNull(appointmentService);
    }

    @Test
    public void getAppointments_empty() {
        AppointmentMeta meta = mock(AppointmentMeta.class);
        when(appointmentSpec.appointmentQuery(meta)).thenReturn(any(Specification.class));
        when(appointmentRepo.findAll(appointmentSpec.appointmentQuery(meta))).thenReturn(new ArrayList<>());
        when(userRepo.existsById(meta.getUserId())).thenReturn(true);

        assertEquals(0, IterableUtil.sizeOf(appointmentService.getAppointments(meta)));
        verify(appointmentRepo, times(1)).findAll(appointmentSpec.appointmentQuery(meta));
    }

    @Test
    public void getAppointments() {
        List<Appointment> appointments = new ArrayList<>();
        appointments.add(mock(Appointment.class));
        appointments.add(mock(Appointment.class));
        AppointmentMeta meta = mock(AppointmentMeta.class);
        when(appointmentSpec.appointmentQuery(meta)).thenReturn(any(Specification.class));
        when(appointmentRepo.findAll(appointmentSpec.appointmentQuery(meta))).thenReturn(appointments);
        when(userRepo.existsById(meta.getUserId())).thenReturn(true);

        assertEquals(appointments.size(), IterableUtil.sizeOf(appointmentService.getAppointments(meta)));
        verify(appointmentRepo, times(1)).findAll(appointmentSpec.appointmentQuery(meta));
    }

    @Test
    public void getAppointments_userNotExist() {
        AppointmentMeta meta = mock(AppointmentMeta.class);
        when(userRepo.existsById(meta.getUserId())).thenReturn(false);

        assertThrows(ResourceNotFoundException.class, () -> appointmentService.getAppointments(meta));
        verify(appointmentRepo, times(0)).findAll(appointmentSpec.appointmentQuery(meta));
    }

    @Test
    public void getAppointmentById() {
        long userId = 1L;
        long aptId = 1L;
        when(appointmentRepo.findByUserUserIdAndAppointmentId(userId, aptId))
                .thenReturn(Optional.of(mock(Appointment.class)));

        Optional<Appointment> userFromService = appointmentService.getAppointmentById(userId, aptId);
        assertTrue(userFromService.isPresent());
        verify(appointmentRepo, times(1)).findByUserUserIdAndAppointmentId(userId, aptId);
    }

    @Test
    public void getAppointmentById_idNotExist() {
        long userId = 1L;
        long aptId = 1L;
        when(appointmentRepo.findByUserUserIdAndAppointmentId(userId, aptId)).thenReturn(Optional.empty());

        Optional<Appointment> userFromService = appointmentService.getAppointmentById(userId, aptId);
        assertTrue(userFromService.isEmpty());
        verify(appointmentRepo, times(1)).findByUserUserIdAndAppointmentId(userId, aptId);
    }

    @Test
    public void getAppointmentById_userNotExist() {
        long userId = 1L;
        long aptId = 1L;
        when(appointmentRepo.findByUserUserIdAndAppointmentId(userId, aptId)).thenReturn(Optional.empty());

        Optional<Appointment> userFromService = appointmentService.getAppointmentById(userId, aptId);
        assertTrue(userFromService.isEmpty());
        verify(appointmentRepo, times(1)).findByUserUserIdAndAppointmentId(userId, aptId);
    }

    @Test
    public void addAppointment() {
        long userId = 1L, aptId = 1L;
        List<Appointment> appointments = new ArrayList<>();
        Appointment apt = mock(Appointment.class);
        appointments.add(apt);
        User user = mock(User.class);
        when(apt.getAppointmentId()).thenReturn(aptId);
        when(user.getAppointments()).thenReturn(appointments);
        when(apt.isComplete()).thenReturn(true);

        when(userRepo.findById(userId)).thenReturn(Optional.of(user));

        assertTrue(appointmentService.addAppointment(userId, apt) > 0);
        verify(userRepo, times(2)).findById(userId);
        verify(userRepo, times(1)).save(user);
    }

    @Test
    public void addAppointment_userNotExist() {
        long userId = 1L;
        Appointment apt = mock(Appointment.class);
        User user = mock(User.class);
        when(userRepo.findById(userId)).thenReturn(Optional.empty());
        when(apt.isComplete()).thenReturn(true);

        assertThrows(ResourceNotFoundException.class, () -> appointmentService.addAppointment(userId, apt));
        verify(userRepo, times(1)).findById(userId);
        verify(userRepo, times(0)).save(user);
    }

    @Test
    public void addAppointment_incomplete() {
        long userId = 1L;
        Appointment apt = mock(Appointment.class);
        User user = mock(User.class);

        when(userRepo.findById(userId)).thenReturn(Optional.of(user));
        when(apt.isComplete()).thenReturn(false);

        assertThrows(BadRequestException.class, () -> appointmentService.addAppointment(userId, apt));
        verify(userRepo, times(1)).findById(userId);
        verify(userRepo, times(0)).save(user);
    }

    @Test
    public void addAppointment_addFailed() {
        long userId = 1L;
        Appointment apt = mock(Appointment.class);
        User user = mock(User.class);
        ArrayList<Appointment> appointments = mock(ArrayList.class);

        when(userRepo.findById(userId)).thenReturn(Optional.of(user));
        when(apt.isComplete()).thenReturn(true);
        when(user.getAppointments()).thenReturn(appointments);
        when(appointments.size()).thenReturn(0);

        assertEquals(-1, appointmentService.addAppointment(userId, apt));
        verify(userRepo, times(2)).findById(userId);
        verify(userRepo, times(1)).save(user);
    }

    @Test
    public void updateAppointment() {
        long userId = 1L;
        long aptId = 1L;
        List<Appointment> appointments = new ArrayList<>();
        Appointment apt = mock(Appointment.class);
        appointments.add(apt);
        User user = mock(User.class);
        when(user.getAppointments()).thenReturn(appointments);
        when(apt.getAppointmentId()).thenReturn(aptId);

        when(userRepo.findById(userId)).thenReturn(Optional.of(user));
        when(appointmentRepo.findById(aptId)).thenReturn(Optional.of(apt));

        assertEquals(aptId, appointmentService.updateAppointment(userId,aptId, apt));
        verify(userRepo, times(1)).findById(userId);
        verify(appointmentRepo, times(1)).findById(aptId);
        verify(userRepo, times(1)).save(user);
    }

    @Test
    public void updateAppointment_aptNotExist() {
        long userId = 1L;
        long aptId = 1L;
        Appointment apt = mock(Appointment.class);
        User user = mock(User.class);

        when(userRepo.findById(userId)).thenReturn(Optional.of(user));
        when(appointmentRepo.findById(aptId)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class,
                () -> appointmentService.updateAppointment(userId,aptId, apt));
        verify(userRepo, times(1)).findById(userId);
        verify(appointmentRepo, times(1)).findById(aptId);
        verify(userRepo, times(0)).save(user);
    }

    @Test
    public void updateAppointment_userNotExist() {
        long userId = 1L;
        long aptId = 1L;
        Appointment apt = mock(Appointment.class);
        User user = mock(User.class);

        when(userRepo.findById(userId)).thenReturn(Optional.empty());
        when(appointmentRepo.findById(aptId)).thenReturn(Optional.of(apt));

        assertThrows(ResourceNotFoundException.class,
                () -> appointmentService.updateAppointment(userId,aptId, apt));
        verify(userRepo, times(1)).findById(userId);
        verify(appointmentRepo, times(0)).findById(aptId);
        verify(userRepo, times(0)).save(user);
    }

    @Test
    public void deleteAppointmentById() {
        long userId = 1L;
        long aptId = 1L;
        Appointment apt = mock(Appointment.class);
        User user = mock(User.class);

        when(userRepo.findById(userId)).thenReturn(Optional.of(user));
        when(appointmentRepo.findById(aptId)).thenReturn(Optional.of(apt));

        assertDoesNotThrow(() -> appointmentService.deleteAppointmentById(userId, aptId));
        verify(userRepo, times(1)).findById(userId);
        verify(appointmentRepo, times(1)).findById(aptId);
        verify(appointmentRepo, times(1)).deleteById(aptId);
    }

    @Test
    public void deleteAppointmentById_aptNotExist() {
        long userId = 1L;
        long aptId = 1L;
        User user = mock(User.class);

        when(userRepo.findById(userId)).thenReturn(Optional.of(user));
        when(appointmentRepo.findById(aptId)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class,
                () -> appointmentService.deleteAppointmentById(userId, aptId));
        verify(userRepo, times(1)).findById(userId);
        verify(appointmentRepo, times(1)).findById(aptId);
        verify(appointmentRepo, times(0)).deleteById(aptId);
    }

    @Test
    public void deleteAppointmentById_userNotExist() {
        long userId = 1L;
        long aptId = 1L;
        Appointment apt = mock(Appointment.class);

        when(userRepo.findById(userId)).thenReturn(Optional.empty());
        when(appointmentRepo.findById(aptId)).thenReturn(Optional.of(apt));


        assertThrows(ResourceNotFoundException.class,
                () -> appointmentService.deleteAppointmentById(userId, aptId));
        verify(userRepo, times(1)).findById(userId);
        verify(appointmentRepo, times(0)).findById(aptId);
        verify(appointmentRepo, times(0)).deleteById(aptId);
    }
}