package com.banaag.personaldev.services;

import com.banaag.personaldev.exception.BadRequestException;
import com.banaag.personaldev.exception.ResourceNotFoundException;
import com.banaag.personaldev.model.Settings;
import com.banaag.personaldev.model.User;
import com.banaag.personaldev.repository.UserRepository;
import org.assertj.core.util.IterableUtil;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

/*
 * NOTE: Do we need to check for items that are inside user? I feel like thats more of a unit test for the model or DB.
 *
 * Spring Boot Test needs db running in back ground. If need to use spring boot test, try using h2 for mocking.
 * */
@ExtendWith(SpringExtension.class)
public class UserServiceUnitTest {

    @InjectMocks
    UserServiceImpl userService;

    @Mock
    UserRepository userRepository;

    @Test
    public void serviceInitTest() {
        assertNotNull(userService);
    }

    @Test
    public void emptyUsersTest() {
        when(userRepository.findAll()).thenReturn(new ArrayList<>());

        assertEquals(0, IterableUtil.sizeOf(userService.getUsers()));
        verify(userRepository, times(1)).findAll();
    }

    @Test
    public void getAllUsers() {
        List<User> users = new ArrayList<>();
        User user = mock(User.class);
        User user2 = mock(User.class);
        users.add(user);
        users.add(user2);

        when(userRepository.findAll()).thenReturn(users);
        assertEquals(users.size(), IterableUtil.sizeOf(userService.getUsers()));
        verify(userRepository, times(1)).findAll();
    }

    @Test
    public void getUserById() {
        long userId = 1L;
        User user = mock(User.class);
        Optional<User> optUser = Optional.of(user);

        when(user.getUserId()).thenReturn(userId);
        when(userRepository.findById(userId)).thenReturn(optUser);

        Optional<User> userFromService = userService.getUserById(userId);
        assertNotNull(userFromService);
        assertTrue(userFromService.isPresent());
        assertEquals(userId, userFromService.get().getUserId());
        verify(userRepository, times(1)).findById(userId);
    }

    @Test
    public void getUserById_idNotExist() {
        long userId = 1L;
        when(userRepository.findById(userId)).thenReturn(Optional.empty());

        Optional<User> userFromService = userService.getUserById(userId);
        assertTrue(userFromService.isEmpty());
        verify(userRepository, times(1)).findById(userId);
    }

    @Test
    public void addUser() {
        User user = mock(User.class);
        when(user.getUserId()).thenReturn(1L);
        when(user.getUsername()).thenReturn("");
        when(user.getEmail()).thenReturn("");

        when(user.isComplete()).thenReturn(true);
        when(userRepository.countByEmailAndUserIdNot(user.getEmail(), user.getUserId())).thenReturn(0);
        when(userRepository.countByUsernameAndUserIdNot(user.getUsername(), user.getUserId())).thenReturn(0);
        when(userRepository.save(user)).thenReturn(user);

        assertEquals(user.getUserId(), userService.addUser(user));
        verify(userRepository, times(1)).save(user);
    }

    @Test
    public void addUser_incompleteBody() {
        User user = mock(User.class);

        when(user.isComplete()).thenReturn(false);

        assertThrows(BadRequestException.class, () -> userService.addUser(user));
        verify(userRepository, times(0)).save(user);
    }

    @Test
    public void addUser_usernameTaken() {
        User user = mock(User.class);

        when(user.isComplete()).thenReturn(true);
        when(user.getUserId()).thenReturn(1L);
        when(user.getUsername()).thenReturn("");
        when(userRepository.countByUsernameAndUserIdNot(user.getUsername(), user.getUserId())).thenReturn(1);

        assertThrows(BadRequestException.class, () -> userService.addUser(user));
        verify(userRepository, times(0)).save(user);
    }

    @Test
    public void addUser_emailTaken() {
        User user = mock(User.class);
        when(user.getUserId()).thenReturn(1L);
        when(user.getUsername()).thenReturn("");
        when(user.getEmail()).thenReturn("");
        when(user.isComplete()).thenReturn(true);

        when(userRepository.countByEmailAndUserIdNot(user.getEmail(), user.getUserId())).thenReturn(1);
        when(userRepository.countByUsernameAndUserIdNot(user.getUsername(), user.getUserId())).thenReturn(0);
        when(userRepository.save(user)).thenReturn(user);

        assertThrows(BadRequestException.class, () -> userService.addUser(user));
        verify(userRepository, times(0)).save(user);
    }

    @Test
    public void updateUser() {
        User user = mock(User.class);
        Settings settings = mock(Settings.class);
        when(settings.isAppAlerts()).thenReturn(true);
        when(settings.isEmailNotifications()).thenReturn(true);
        when(user.getSettings()).thenReturn(settings);
        when(settings.getTheme()).thenReturn(Settings.Themes.DARK);
        when(user.getUserId()).thenReturn(1L);
        when(user.getUsername()).thenReturn("");
        when(user.getEmail()).thenReturn("");

        when(userRepository.findById(user.getUserId())).thenReturn(Optional.of(user));
        when(userRepository.countByEmailAndUserIdNot(user.getEmail(), user.getUserId())).thenReturn(0);
        when(userRepository.countByUsernameAndUserIdNot(user.getUsername(), user.getUserId())).thenReturn(0);
        when(userRepository.save(user)).thenReturn(user);

        assertEquals(user.getUserId(), userService.updateUser(user.getUserId(), user));
        verify(userRepository, times(1)).save(user);
    }

    @Test
    public void updateUser_idNotExist() {
        User user = mock(User.class);
        when(user.getUserId()).thenReturn(1L);
        when(userRepository.findById(user.getUserId())).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> userService.updateUser(user.getUserId(), user));
        verify(userRepository, times(0)).save(user);
    }

    @Test()
    public void updateUser_usernameTaken() {
        User user = mock(User.class);

        when(user.getUserId()).thenReturn(1L);
        when(user.getUsername()).thenReturn("");
        when(userRepository.findById(user.getUserId())).thenReturn(Optional.of(user));
        when(userRepository.countByUsernameAndUserIdNot(user.getUsername(), user.getUserId())).thenReturn(1);

        assertThrows(BadRequestException.class, () -> userService.updateUser(user.getUserId(), user));
        verify(userRepository, times(0)).save(user);
    }

    @Test
    public void updateUser_emailTaken() {
        User user = mock(User.class);

        when(user.getUserId()).thenReturn(1L);
        when(user.getUsername()).thenReturn("");
        when(user.getEmail()).thenReturn("");
        when(userRepository.findById(user.getUserId())).thenReturn(Optional.of(user));
        when(userRepository.countByUsernameAndUserIdNot(user.getUsername(), user.getUserId())).thenReturn(0);
        when(userRepository.countByEmailAndUserIdNot(user.getEmail(), user.getUserId())).thenReturn(1);

        assertThrows(BadRequestException.class, () -> userService.updateUser(user.getUserId(), user));
        verify(userRepository, times(0)).save(user);
    }

    @Test
    public void deleteUserById() {
        when(userRepository.existsById(1L)).thenReturn(true);

        userService.deleteUserById(1L);

        verify(userRepository, times(1)).deleteById(1L);
    }

    @Test
    public void deleteUserById_idNotExist() {
        when(userRepository.existsById(1L)).thenReturn(false);

        assertThrows(ResourceNotFoundException.class, () -> userService.deleteUserById(1L));
        verify(userRepository, times(0)).deleteById(1L);
    }
}
